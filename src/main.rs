extern crate aum_parser;

use aum_parser::tokenization::parse_source;
use std::io::{self, BufRead, Write};

fn main() {
    let stdin = io::stdin();

    loop {
        print!("λ ");
        io::stdout().flush().expect("Error flushing stdout");

        let mut line = String::new();
        stdin
            .lock()
            .read_line(&mut line)
            .expect("Error reading from stdin");

        for tok in parse_source(&mut line) {
            println!("{:?}", tok);
        }
    }
}
